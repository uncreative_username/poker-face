import serial
import time
import threading
from tkinter import *
from tkinter import font

comPort = "COM11"
baud = 115200
serBuffer = "0,0" #buffer for incoming serial data
targetScore = "30"
team1Score = "0"
team2Score = "0"
start = 0
fullscreen = 0

def toggleFullscreen(*args):
	global fullscreen
	if fullscreen:
		gui.attributes('-fullscreen', False)
		fullscreen = 0
		gui.config(menu=menu)
	else:
		gui.attributes('-fullscreen', True)
		fullscreen = 1
		emptyMenu = Menu(gui) #hide menu
		gui.config(menu=emptyMenu)

#create main window
gui = Tk()
gui.title("Poker Face")
gui.geometry('350x200')
gui.bind("<F11>", toggleFullscreen)

comNumIn = StringVar()
targetScoreIn = StringVar()
scoreIn = StringVar()

ser = serial.Serial()

#stop arduino fom reseting on connect
# ser.rts = False
# ser.dtr = False

def setPort():
	global comPort, ser
	print("setPort")
	ser.close()             # close previous port
	try:
		ser = serial.Serial(comPort, baudrate=baud, timeout=1, writeTimeout=1)  # open serial port
		print("using :" + ser.name)         # check which port was really used
		ser.write(b'F')
	except serial.serialutil.SerialException:
		print('Cannot open port')
	time.sleep(0.5)
setPort()


def setComPort(*args):
	global comPort
	global ser
	print("setComPort")
	if comNumIn.get():
		comPort = comNumIn.get()
	print("new com port: " + comPort)
	setPort()
	
def setTargetScore(*args):
	global targetScore
	targetScore = targetScoreIn.get()	
	print("new target score: "+ targetScore)
	
def setScore(*args):
	global serBuffer
	global startBtn
	global start
	start = 1
	serBuffer = scoreIn.get()
	#getScore()
	# splitStr = serBuffer.split(',')
	try:
		ser.write(serBuffer.encode())
		# ser.write(b'D')
		# ser.write(splitStr[0].encode())
		# ser.write(b'E')
		# ser.write(splitStr[1].encode())
	except:
		setPort()
		print("port error, retrying")
	print("Set score: "+ serBuffer)
	startBtn.config(text="stop", command=restartGame) #change to stop button
	print("start")

def changeSettings():
	setWin = Toplevel(gui)
	setWin.focus_set()
	#setWin.grab_set()
	
	comNumEntry = Entry(setWin, textvariable=comNumIn)
	comNumIn.set(comPort)
	comNumEntry.grid(column=0, row=0, padx=5, pady=5)
	comNumEntry.bind("<Return>", setComPort)
	
	btn1 = Button(setWin, text="Set COM port", command=setComPort)
	btn1.grid(column=1, row=0, padx=5, pady=5, sticky="w")
	
	targetScoreEntry = Entry(setWin, textvariable=targetScoreIn)
	targetScoreIn.set(targetScore)
	targetScoreEntry.grid(column=0, row=1, padx=5, pady=5, sticky="w")
	targetScoreEntry.bind("<Return>", setTargetScore)
	
	btn2 = Button(setWin, text="Set winning score", command=setTargetScore)
	btn2.grid(column=1, row=1, padx=5, pady=5)
	
	setScoreEntry = Entry(setWin, textvariable=scoreIn)
	scoreIn.set("0,0")
	setScoreEntry.grid(column=0, row=2, padx=5, pady=5, sticky="w")
	setScoreEntry.bind("<Return>", setScore)
	
	btn3 = Button(setWin, text="Set score", command=setScore)
	btn3.grid(column=1, row=2, padx=5, pady=5)
	

#settings menu
menu = Menu(gui)
menu.add_command(label="Settings", command=changeSettings)
gui.config(menu=menu)

#change font size when window size changes
def resize(event):
	width = gui.winfo_width()
	height = gui.winfo_height()
	
	if width < height:
		nameSize = int(width/-8)
		scoreSize = int(width/-3)
	else:
		nameSize = int(width/-8)
		scoreSize = int(width/-3)
	
	nameFont = font.Font(size=nameSize)
	scoreFont = font.Font(size=scoreSize)
	btnFont = font.Font(size=int(height/20))
	
	left_frame.resizeFont(nameFont, scoreFont)
	right_frame.resizeFont(nameFont, scoreFont)
	
	winLabel.config(font=nameFont)
	
	startBtn.config(font=btnFont)
	restartBtn.config(font=btnFont)
	
gui.bind("<Configure>", resize)

class ScoreFrame:
	def __init__(self, parent, color, col, row, team_name):
		self.scoreVal = 0
		self.frame = Frame(parent)
		self.frame.grid(column=col, row=row, sticky="nsew")
		
		self.teamName = Label(self.frame, text=team_name, fg=color)
		self.teamName.grid(column=1, row=0, sticky="n")
		
		self.Score = Label(self.frame, text="0", fg=color)
		self.Score.grid(column=1, row=1, sticky="n")
		
		#create 3x3 grid
		for x in range(0, 3):
			self.frame.rowconfigure(x, weight=1)
			self.frame.columnconfigure(x, weight=1)
	
	def changeScore(self, score):
		self.scoreVal = score
		self.Score.configure(text=str(self.scoreVal))
		
	def resizeFont(self, nameFont, scoreFont):
		self.teamName.config(font=nameFont)
		self.Score.config(font=scoreFont)

left_frame = ScoreFrame(gui, "red", 0, 0, "Team 1")
right_frame = ScoreFrame(gui, "black", 1, 0, "Team 2")

#count up score for testing
score = 0
def incScore():
	global score
	global serBuffer
	while True:
		if start:
			score = score+1
			serBuffer = str(score) + ',' + str(score+10)
			serBuffer = serBuffer.encode()
			print(serBuffer)
			getScore()
			# left_frame.changeScore(score)
			# right_frame.changeScore(score+1)
		else:
			score = 0
		time.sleep(1) #do every second
			#gui.after(1000, incScore)
#incScore
#get string from serial port
def readSerial():
	global serBuffer
	while True:
		try:
			serBuffer =  ser.readline()[:-2]
			#print("opened port: " + ser.name)         # check which port was really used
			if serBuffer:
				print("received: " + serBuffer.decode())
				if b',' in serBuffer:
					getScore()
		except:
			setPort()
			print("readSerial port error, retrying")
		#time.sleep(0.01)
	# gui.after(10, readSerial)
# gui.after(10,readSerial)

#receive score from arduino and update on screen
def getScore():
	global team1Score
	global team2Score
	global serBuffer
	serBuffer = serBuffer.decode()
	splitStr = serBuffer.split(',')
	print(splitStr)
	team1Score = splitStr[0]
	team2Score = splitStr[1]
	left_frame.changeScore(team1Score)
	right_frame.changeScore(team2Score)
	
def startGame():
	global startBtn
	global start
	start = 1
	# thread1.start() #for testing with incScore()
	try:
		ser.write(b'S')
	except:
		setPort()
		print("port error, retrying")
	startBtn.config(text="stop", command=restartGame) #change to stop button
	print("start")
startBtn = Button(gui, text="start", command=startGame)
startBtn.grid(column=0, row=0, columnspan=2, sticky="s", padx=5, pady=5)

def restartGame():
	global team1Score
	global team2Score
	global serBuffer
	global startBtn
	team1Score = 0
	team2Score = 0
	serBuffer = "0,0"
	left_frame.changeScore(team1Score)
	right_frame.changeScore(team2Score)
	winFrame.grid_remove() #hide frame
	startBtn.config(text="start", command=startGame) #change back to start button
	try:
		ser.write(b'F')
	except:
		setPort()
		print("restartGame port error, retrying")
	print("restart")
	
winFrame = Frame(gui)
winFrame.grid(column=0, row=0, columnspan=2, sticky="nsew")
winFrame.grid_remove() #hide frame
winFrame.rowconfigure(0, weight=1)
winFrame.columnconfigure(0, weight=1)
winLabel = Label(winFrame)
winLabel.grid()
restartBtn = Button(winFrame, text="Restart game", command=restartGame)
restartBtn.grid(padx=5, pady=5)

def dispWinner():
	global start
	global ser
	while True:
		if start:
			# print (type(team1Score))
			# print (team1Score)
			# print (type(team2Score))
			# print (team2Score)
			# print (type(targetScore))
			# print (targetScore)
			if team1Score == targetScore:
				winLabel.config(text="Team 1 wins")
				winFrame.grid()
				start = 0
				try:
					ser.write(b'F')
				except:
					setPort()
					print("dispWinner port error, retrying")
			elif team2Score == targetScore:
				winLabel.config(text="Team 2 wins")
				winFrame.grid()
				start = 0
				try:
					ser.write(b'F')
				except:
					setPort()
					print("dispWinner port error, retrying")
		time.sleep(0.01)
	
	
thread1 = threading.Thread(target=incScore, daemon=True)

thread2 = threading.Thread(target=readSerial, daemon=True)
thread2.start()

thread3 = threading.Thread(target=dispWinner, daemon=True)
thread3.start()

gui.rowconfigure(0, weight=1)
gui.columnconfigure(0, weight=1)
gui.columnconfigure(1, weight=1)

gui.mainloop()