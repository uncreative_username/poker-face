/*
  ----------------------------------
              MFRC522      Arduino
              Reader/PCD   Uno/101
  Signal      Pin          Pin
  ----------------------------------
  RST/Reset   RST          9
  SPI SS 1    SDA(SS)      10
  SPI SS 2    SDA(SS)      8
  SPI SS 3    SDA(SS)      7
  SPI SS 4    SDA(SS)      6
  SPI MOSI    MOSI         11
  SPI MISO    MISO         12
  SPI SCK     SCK          13

*/
#define DEBUG

#include <SPI.h>
#include <MFRC522.h>


constexpr uint8_t RST_PIN = 9;
constexpr uint8_t SS_1_PIN = 10;
constexpr uint8_t SS_2_PIN = 8;




constexpr uint8_t NR_OF_READERS = 2;

byte ssPins[] = {SS_1_PIN, SS_2_PIN};

MFRC522 mfrc522[NR_OF_READERS];   // Create MFRC522 instance.

String UIDstring, UIDstring1, UIDstring2;
int score2 = 0;
int score1 = 0;
byte to_update = 1;
byte card_used[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
byte start = 0;
String incoming;
/**
   Initialize.
*/
void setup() {


  randomSeed(analogRead(0));
  Serial.begin(115200); // Initialize serial communications with the PC
  //while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)

  SPI.begin();        // Init SPI bus

  for (uint8_t reader = 0; reader < NR_OF_READERS; reader++) {
    mfrc522[reader].PCD_Init(ssPins[reader], RST_PIN); // Init each MFRC522 card
#ifdef DEBUG
    pinMode(7, INPUT_PULLUP);
    Serial.print(F("Reader "));
    Serial.print(reader + 1);
    Serial.print(F(": "));
#endif
    mfrc522[reader].PCD_DumpVersionToSerial();

    //mfrc522[reader].PCD_SetAntennaGain(0x07<<4);

  }
}

/**
   Main loop.
*/
void loop()
{

#ifdef DEBUG
  if(!digitalRead(7))
  {
    //increase score when putton pressed for testing
    while(!digitalRead(7)); //wait for release
    score1++;
    to_update = 1;
  }
    
#endif

  if (Serial.available() > 0) {

    incoming = Serial.readString();
    //Serial.println(incoming);
    // read the incoming byte:
    if(incoming.length()>1)
    {
      score1 =(incoming.substring(0, incoming.indexOf(','))).toInt();
      score2 = (incoming.substring(incoming.indexOf(',')+1)).toInt();
      to_update = 1;
      start = 1;
      for (int i = 0; i < 56 ; i++)
      {
        card_used[i] = 1;
      }
    }
    else
    {
      if (incoming == "S")
      {
        start = 1;
        score2 = 0;
        score1 = 0;
        to_update = 1;
        for (int i = 0; i < 56 ; i++)
        {
          card_used[i] = 1;
        }
  
        Serial.println(F("Arduino Received S"));
        
      }
      if (incoming == "F")
      {
        start = 0;
        Serial.println(F("Arduino Received F"));
      }
      incoming = "";
    }
  }

  if (start == 1)
  {
    for (uint8_t reader = 0; reader < NR_OF_READERS; reader++)
    {
      // Look for new cards
      if (mfrc522[reader].PICC_IsNewCardPresent() && mfrc522[reader].PICC_ReadCardSerial()) {
#ifdef DEBUG
        Serial.print(F("Reader "));
        Serial.print(reader + 1);
        // Show some details of the PICC (that is: the tag/card)
        Serial.print(F(": Card UID:"));
#endif
        dump_byte_array(mfrc522[reader].uid.uidByte, mfrc522[reader].uid.size);
#ifdef DEBUG
        Serial.println(UIDstring);
#endif
        if (reader == 0)
        {
          UIDstring1 = UIDstring;
        }
        else if (reader == 1)
        {
          UIDstring2 = UIDstring;
        }

        // Halt PICC
        mfrc522[reader].PICC_HaltA();
        // Stop encryption on PCD
        mfrc522[reader].PCD_StopCrypto1();
      }
    }

    if (UIDstring1.length() != 0)
    {
      score1 += return_score(UIDstring1);
      if (score1 < 0)
      {
        score1 = 0;
      }
      //Serial.print(return_score(UIDstring1));
      //Serial.print("Score 1 :");
      //Serial.println(score1);
      UIDstring1 = "";
      to_update = 1;
    }
    if (UIDstring2.length() != 0)
    {
      score2 += return_score(UIDstring2);
      if (score2 < 0)
      {
        score2 = 0;
      }
      //Serial.print(return_score(UIDstring2));
      //Serial.print("Score 2 :");
      //Serial.println(score2);
      UIDstring2 = "";
      to_update = 1;
    }

  }
  if (to_update)
  {
#ifdef DEBUG
    Serial.print(F("Score 1 :"));
    Serial.println(score1);
    Serial.print(F("Score 2 :"));
    Serial.println(score2);
#endif
    Serial.print(score1);
    Serial.print(",");
    Serial.println(score2);
    to_update = 0;
  }
}

/**
   Helper routine to dump a byte array as hex values to Serial.
*/
int return_score(String id)
{
  //Ace of Diamonds
  if (id == F("49151226145101128"))
  {
    if (card_used[0] == 1)
    {
      card_used[0] = 0;
      return -1;
    }
  }
  //Two of Diamonds
  if (id == F("435175218142101129"))
  {
    if (card_used[1] == 1)
    {
      card_used[1] = 0;
      return -2;
    }
  }
  //Three of Diamonds
  if (id == F("431175218142101129"))
  {
    if (card_used[2] == 1)
    {
      card_used[2] = 0;
      return -3;
    }
  }
  //Four of Diamonds
  if (id == F("427175218142101129"))
  {
    if (card_used[3] == 1)
    {
      card_used[3] = 0;
      return -4;
    }
  }
  //Five of Diamonds
  if (id == F("423175218142101129"))
  {
    if (card_used[4] == 1)
    {
      card_used[4] = 0;
      return -5;
    }
  }
  //Six of Diamonds
  if (id == F("419175218142101129"))
  {
    if (card_used[5] == 1)
    {
      card_used[5] = 0;
      return -6;
    }
  }
  //Seven of Diamonds
  if (id == F("433151226145101128"))
  {
    if (card_used[6] == 1)
    {
      card_used[6] = 0;
      return -7;
    }
  }
  //Eight of Diamonds
  if (id == F("436152226145101128"))
  {
    if (card_used[7] == 1)
    {
      card_used[7] = 0;
      return -8;
    }
  }
  //Nine of Diamonds
  if (id == F("440152226145101128"))
  {
    if (card_used[8] == 1)
    {
      card_used[8] = 0;
      return -9;
    }
  }
  //Tenth of Diamonds
  if (id == F("444152226145101128"))
  {
    if (card_used[9] == 1)
    {
      card_used[9] = 0;
      return -10;
    }
  }
  //Jack of Diamonds
  if (id == F("448152226145101128"))
  {
    if (card_used[10] == 1)
    {
      card_used[10] = 0;
      return -11;
    }
  }
  //Queen of Diamonds
  if (id == F("461326152101133"))
  {
    if (card_used[11] == 1)
    {
      card_used[11] = 0;
      return -12;
    }
  }
  //King of Diamonds
  if (id == F("421326152101133"))
  {
    if (card_used[12] == 1)
    {
      card_used[12] = 0;
      return -13;
    }
  }
  //Ace of Clubs
  if (id == F("41881226152101132"))
  {
    if (card_used[13] == 1)
    {
      card_used[13] = 0;
      return 1;
    }
  }
  //Two of Clubs
  if (id == F("41841226152101132"))
  {
    if (card_used[14] == 1)
    {
      card_used[14] = 0;
      return 2;
    }
  }
  //Three of Clubs
  if (id == F("41801226152101132"))
  {
    if (card_used[15] == 1)
    {
      card_used[15] = 0;
      return 3;
    }
  }
  //Four of Clubs
  if (id == F("41761226152101132"))
  {
    if (card_used[16] == 1)
    {
      card_used[16] = 0;
      return 4;
    }
  }
  //Five of Clubs
  if (id == F("42351226152101132"))
  {
    if (card_used[17] == 1)
    {
      card_used[17] = 0;
      return 5;
    }
  }
  //Six of Clubs
  if (id == F("42311226152101132"))
  {
    if (card_used[18] == 1)
    {
      card_used[18] = 0;
      return 6;
    }
  }
  //Seven of Clubs
  if (id == F("42271226152101132"))
  {
    if (card_used[19] == 1)
    {
      card_used[19] = 0;
      return 7;
    }
  }
  //Eight of Clubs
  if (id == F("42231226152101132"))
  {
    if (card_used[20] == 1)
    {
      card_used[20] = 0;
      return 8;
    }
  }
  //Nine of Clubs
  if (id == F("42191226152101132"))
  {
    if (card_used[21] == 1)
    {
      card_used[21] = 0;
      return 9;
    }
  }
  //Tenth of Clubs
  if (id == F("429151226145101128"))
  {
    if (card_used[22] == 1)
    {
      card_used[22] = 0;
      return 10;
    }
  }
  //Jack of Clubs
  if (id == F("4145246226160101128"))
  {
    if (card_used[23] == 1)
    {
      card_used[23] = 0;
      return 11;
    }
  }
  //Queen of Clubs
  if (id == F("4140246226160101128"))
  {
    if (card_used[24] == 1)
    {
      card_used[24] = 0;
      return 12;
    }
  }
  //King of Clubs
  if (id == F("413151226145101128"))
  {
    if (card_used[25] == 1)
    {
      card_used[25] = 0;
      return 13;
    }
  }
  //Ace of Hearts
  if (id == F("41521226152101132"))
  {
    if (card_used[26] == 1)
    {
      card_used[26] = 0;
      return -1;
    }
  }
  //Two of Hearts
  if (id == F("41591226152101132"))
  {
    if (card_used[27] == 1)
    {
      card_used[27] = 0;
      return -2;
    }
  }
  //Three of Hearts
  if (id == F("41711226152101132"))
  {
    if (card_used[28] == 1)
    {
      card_used[28] = 0;
      return -3;
    }
  }
  //Four of Hearts
  if (id == F("41671226152101132"))
  {
    if (card_used[29] == 1)
    {
      card_used[29] = 0;
      return -4;
    }
  }
  //Five of Hearts
  if (id == F("41631226152101132"))
  {
    if (card_used[30] == 1)
    {
      card_used[30] = 0;
      return -5;
    }
  }
  //Six of Hearts
  if (id == F("41601126152101132"))
  {
    if (card_used[31] == 1)
    {
      card_used[31] = 0;
      return -6;
    }
  }
  //Seven of Hearts
  if (id == F("41521126152101132"))
  {
    if (card_used[32] == 1)
    {
      card_used[32] = 0;
      return -7;
    }
  }
  //Eight of Hearts
  if (id ==F("41481126152101132"))
  {
    if (card_used[33] == 1)
    {
      card_used[33] = 0;
      return -8;
    }
  }
  //Nine of Hearts
  if (id ==F("41441126152101132"))
  {
    if (card_used[34] == 1)
    {
      card_used[34] = 0;
      return -9;
    }
  }
  //Tenth of Hearts
  if (id ==F("41401126152101132"))
  {
    if (card_used[35] == 1)
    {
      card_used[35] = 0;
      return -10;
    }
  }
  //Jack of Hearts
  if (id ==F("41361126152101132"))
  {
    if (card_used[36] == 1)
    {
      card_used[36] = 0;
      return -11;
    }
  }
  //Queen of Hearts
  if (id ==F("41321126152101132"))
  {
    if (card_used[37] == 1)
    {
      card_used[37] = 0;
      return -12;
    }
  }
  //King of Hearts
  if (id ==F("41921226152101132"))
  {
    if (card_used[38] == 1)
    {
      card_used[38] = 0;
      return -13;
    }
  }
  //Ace of Spades
  if (id ==F("41821326152101132"))
  {
    if (card_used[39] == 1)
    {
      card_used[39] = 0;
      return 1;
    }
  }
  //Two of Spades
  if (id ==F("41861326152101132"))
  {
    if (card_used[40] == 1)
    {
      card_used[40] = 0;
      return 2;
    }
  }
  //Three of Spades
  if (id ==F("41901326152101132"))
  {
    if (card_used[41] == 1)
    {
      card_used[41] = 0;
      return 3;
    }
  }
  //Four of Spades
  if (id ==F("41941326152101132"))
  {
    if (card_used[42] == 1)
    {
      card_used[42] = 0;
      return 4;
    }
  }
  //Five of Spades
  if (id ==F("41981326152101132"))
  {
    if (card_used[43] == 1)
    {
      card_used[43] = 0;
      return 5;
    }
  }
  //Six of Spades
  if (id ==F("41281126152101132"))
  {
    if (card_used[44] == 1)
    {
      card_used[44] = 0;
      return 6;
    }
  }
  //Seven of Spades
  if (id ==F("41241126152101132"))
  {
    if (card_used[45] == 1)
    {
      card_used[45] = 0;
      return 7;
    }
  }
  //Eight of Spades
  if (id ==F("41271226152101132"))
  {
    if (card_used[46] == 1)
    {
      card_used[46] = 0;
      return 8;
    }
  }
  //Nine of Spades
  if (id ==F("41311226152101132"))
  {
    if (card_used[47] == 1)
    {
      card_used[47] = 0;
      return 9;
    }
  }
  //Tenth of Spades
  if (id ==F("41351226152101132"))
  {
    if (card_used[48] == 1)
    {
      card_used[48] = 0;
      return 10;
    }
  }
  //Jack of Spades
  if (id ==F("41391226152101132"))
  {
    if (card_used[49] == 1)
    {
      card_used[49] = 0;
      return 11;
    }
  }
  //Queen of Spades
  if (id ==F("41441226152101132"))
  {
    if (card_used[50] == 1)
    {
      card_used[50] = 0;
      return 12;
    }
  }
  //King of Spades
  if (id ==F("41481226152101132"))
  {
    if (card_used[51] == 1)
    {
      card_used[51] = 0;
      return 13;
    }
  }
  //Red Joker 1
  if (id ==F("42481326152101132" ))
  {
    if (card_used[52] == 1)
    {
      card_used[52] = 0;
      return random(-13, 0);
    }
  }
  //Black Joker 1
  if (id ==F("492152226145101128"  ))
  {
    if (card_used[53] == 1)
    {
      card_used[53] = 0;
      return random(0, 13);
    }
  }
  //Red Joker 2
  if (id ==F("42521326152101132"))
  {
    if (card_used[54] == 1)
    {
      card_used[54] = 0;
      return random(-13, 0);
    }
  }
  //Black Joker 2
  if ( id ==F("42441326152101132"))
  {
    if (card_used[55] == 1)
    {
      card_used[55] = 0;
      return random(0, 13);
    }
  }

  return 0;
}
void dump_byte_array(byte *buffer, byte bufferSize)
{

  UIDstring = "";
  for (byte i = 0; i < bufferSize; i++) {

    UIDstring = UIDstring + String(buffer[i]);
#ifdef DEBUG
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
#endif
  }
#ifdef DEBUG
  Serial.println();
#endif
  /*Serial.println(UIDstring);
    Serial.print("Card 1: ");
    Serial.println(UIDstring1);
    Serial.print("Card 2: ");
    Serial.println(UIDstring2);
    Serial.print("Card 3: ");
    Serial.println(UIDstring3);
    Serial.print("Card 4: ");
    Serial.println(UIDstring4);
    if (UIDstring == Card1)
    {
    Serial.println("I Found Card 1");
    }
    else if (UIDstring == Card2)
    {
    Serial.println("I Found Card 2");
    }
    else if (UIDstring == Card3)
    {
    Serial.println("I Found Card 3");
    }
    else if (UIDstring == Card4)
    {
    Serial.println("I Found Card 4");
    }
    else if (UIDstring == Card5)
    {
    Serial.println("I Found Card 5");
    }*/

}

/*void CheckCardStillThere(uint8_t reader)
  {
    //for (int i = 0; i < 3; i++) {
      // Detect Tag without looking for collisions

      byte bufferATQA[2];
      byte bufferSize = sizeof(bufferATQA);

      MFRC522::StatusCode result = mfrc522[reader].PICC_WakeupA(bufferATQA, &bufferSize);
      Serial.print(F("Reader "));
      Serial.print(reader+1);
     Serial.print(F(":"));
      dump_byte_array(mfrc522[reader].uid.uidByte, mfrc522[reader].uid.size);

      if (result == mfrc522[reader].STATUS_OK && mfrc522[reader].PICC_ReadCardSerial() && (UIDstring == *UIDArray[reader])) {
        mfrc522[reader].PICC_HaltA();
        //Serial.println("Izzit i still detecting");
        return;
      }

    //}

    //include removal of shit because card not there le.
  }*/
